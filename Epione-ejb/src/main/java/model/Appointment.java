package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Appointments database table.
 * 
 */
@Entity
@Table(name="Appointments")
@NamedQuery(name="Appointment.findAll", query="SELECT a FROM Appointment a")
public class Appointment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int appointmentId;

	private String dateAppointment;

	private String datePriseRDV;

	@Column(name="HourAppointment")
	private int hourAppointment;

	private String message;

	private int note;

	private float price;

	private String reason;

	private String state;

	//bi-directional many-to-one association to Path
	@ManyToOne
	private Path path;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="Iddoctor")
	private User user1;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="IdPatient")
	private User user2;

	//bi-directional one-to-one association to Raiting
	@OneToOne(mappedBy="appointment")
	private Raiting raiting;

	public Appointment() {
	}

	public int getAppointmentId() {
		return this.appointmentId;
	}

	public void setAppointmentId(int appointmentId) {
		this.appointmentId = appointmentId;
	}

	public String getDateAppointment() {
		return this.dateAppointment;
	}

	public void setDateAppointment(String dateAppointment) {
		this.dateAppointment = dateAppointment;
	}

	public String getDatePriseRDV() {
		return this.datePriseRDV;
	}

	public void setDatePriseRDV(String datePriseRDV) {
		this.datePriseRDV = datePriseRDV;
	}

	public int getHourAppointment() {
		return this.hourAppointment;
	}

	public void setHourAppointment(int hourAppointment) {
		this.hourAppointment = hourAppointment;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getNote() {
		return this.note;
	}

	public void setNote(int note) {
		this.note = note;
	}

	public float getPrice() {
		return this.price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Object getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Path getPath() {
		return this.path;
	}

	public void setPath(Path path) {
		this.path = path;
	}

	public User getUser1() {
		return this.user1;
	}

	public void setUser1(User user1) {
		this.user1 = user1;
	}

	public User getUser2() {
		return this.user2;
	}

	public void setUser2(User user2) {
		this.user2 = user2;
	}

	public Raiting getRaiting() {
		return this.raiting;
	}

	public void setRaiting(Raiting raiting) {
		this.raiting = raiting;
	}

}